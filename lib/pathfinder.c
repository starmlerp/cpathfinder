// cpathfinder is free software: you can redistribute it and/or modify it under the
// terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later version.
//
// cpathfinder is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
// PARTICULAR PURPOSE.  See the GNU General Public License for more details.

#include "pathfinder.h"

bool debug = false;

void pathfDebugMode(bool state){
	bool last = debug;
	debug=state;
	if(debug != last){
		if(debug)printf("cpathfinder v0.1.1 -indev- \n code written and maintained by: Filip Jamuljak");
		if(debug)printf("\ncpathfinder is free software: you can redistribute it and/or modify it under the");
		if(debug)printf("\nterms of the GNU General Public License as published by the Free Software");
		if(debug)printf("\nFoundation, either version 3 of the License, or (at your option) any later version.\n");

		if(debug)printf("\ncpathfinder is distributed in the hope that it will be useful, but WITHOUT ANY\n");
		if(debug)printf("WARRANTY: without even the implied warranty of MERCHANTABILITY or FITNESS FOR A\n");
		if(debug)printf("PARTICULAR PURPOSE.  See the GNU General Public License for more details.\n");
	
		if(debug)printf("\nWARNING: code is running in DEBUG MODE. this means it will output messages at vital breakpoints\n");
		if(debug)printf("SYSTEM: if you want to turn debug mode off, call 'DebugMode(false);'\n");
		if(!debug)printf("WARNING: debug mode has been turned off. To turn it on, call 'DebugMode(true);'.\n");
	}
}


void printArray(Array out){
	if(debug){
		if(out.size != 0){
			printf("[");
			for(int i=0; i < out.size; i++){
				printf("%lu%s",out.array[i],i<out.size?", ":"]");
			}
		}
		else printf("[]");
		fflush(stdin);
	}
}
void printNode(Node node){
	if(debug)printf("ID=%lu, x=%d, y=%d, adjacent=", node.id, node.x, node.y);
	printArray(node.adjacent);
}
void printNetwork(Network net){
	if(debug)for(unsigned long i = 0; i < net.size; i++){
		printf("%lu",i);
		printNode(net.network[i]);
		printf("\n");
	}
}

Network readGraphf(char* file){
	FILE* f=fopen(file,"r");
	unsigned long start, end, r;
	if(f!=NULL){
		unsigned long start, end, r;
		printf("SYSTEM: reading file %s...\n", file);
		fscanf(f,"%lu %lu %lu",&start, &end, &r);
		printf("SYSTEM: file input obtained: start node ID is %lu, end node ID is %lu, network is %lu node%s big\n",start, end, r,(r>1)?"s":"");
		Network net=initNetwork();
		setNetwork(&net, r);

		for(unsigned long i = 0; i < r; i++){
			unsigned long id,size;
			int x, y;
			printf("SYSTEM: reading file for node %lu\n",i);
			fscanf(f, "%lu %d %d %lu", &id, &x, &y, &size);
			printf("SYSTEM: file input obtained:ID=%lu, x=%d, y=%d; node is adjacent to %lu node%s\n", id, x, y, size, (size>1)?"s":"");
			Node node=initNode();
			setNode(&node, id, x, y, size);
			printf("SYSTEM: adjacent nodes: ");
			printArray(node.adjacent);
			net.network[i]=node;
		}
		return net;
	}
	else return initNetwork();
}

Tracker initTracker(Node node){
	Tracker tracker;
	tracker.visited=false;
	tracker.node=node.id;
	tracker.weight=-1;
	tracker.origin=0;
	return tracker;
}

Tracker* findTracker(Map map, unsigned long node){
	for(int i=0; i < map.size; i++){
		if(map.map[i].node == node)return &map.map[i];
	}
	return NULL;
}

float getWeight(Tracker tracker, Tracker origin, Network net){
	if(origin.weight >= 0 && tracker.node != 0){
		Node* Tnode=getNodeByIndex(&net, tracker.node);
		Node* Onode=getNodeByIndex(&net, origin.node );
		return origin.weight + distance(Onode->x, Onode->y, Tnode->x, Tnode->y);
	}
	return -1;
}

Map initMap(){
	Map map;
	map.size=0;
	map.map=NULL;
	return map;
}

Map setupMap(Network net){
	Map map = initMap();
	map.size=net.size;
	map.map=(Tracker*)malloc(net.size * sizeof(Tracker));
	for(int i = 0; i < map.size; i++){
		map.map[i]=initTracker(net.network[i]);
	}
	return map;
}

void setArray(Array* array, unsigned long size){
	if(debug)printf("SYSTEM: setArray has been called with input value of %lu\n", size);
	if(array->size > 0 || array->array!=NULL){
		if(debug)printf("WARNING: input array has already been allocated\n");
		if(debug)printf("ATTENTION: deallocating input array\n");
		free(array->array);
	}
	array->size=size;
	if(size > 0)array->array = (unsigned long*)malloc(size * sizeof(unsigned long));
	else{
		array->array = NULL;
		if(debug)printf("WARNING: array has not been allocated because input value is zero\n");
	}
	if(size > 0 && array->array==NULL && debug)printf("WARNING: failed to allocate requested array\n");
	else if(debug)printf("SYSTEM: setArray has finished successfully\n");
}
Array initArray(){
	Array array;
	array.size=0;
	array.array=NULL;
	return array;
}
void setNode(Node* node, unsigned long id, int x, int y, unsigned long size){
	if(debug)printf("SYSTEM: setNode has been called with following input values: ");
	if(debug)printf("	id: %lu, x: %d, y: %d, size: %lu\n",id, x, y, size);
	node->id=id;
	node->x=x;
	node->y=y;
	if(debug)printf("SYSTEM: initializing array containing adjacent nodes\n");
	setArray(&node->adjacent, size);
	if(debug)printf("SYSTEM: setNode has finished successfully\n");
}
Node initNode(){
	Node node;
	node.id=0;
	node.x=0;
	node.y=0;
	node.adjacent=initArray();
	return node;
}
void setNetwork(Network* net, unsigned long size){
	if(debug)printf("SYSTEM: setNetwork has been called with input value of %lu\n",size);

	if(net->size > 0 || net->network!=NULL){		
		if(debug)printf("WARNING: input network has already been allocated\n");
		if(debug)printf("SYSTEM: deallocating input network\n");
		free(net->network);
	}

	net->size=size;
	if(size > 0)net->network=(Node*)malloc(size * sizeof(Node));
	else {
		net->network=NULL;
		if(debug)printf("WARNING: network has not been allocated because input value is 0\n");
	}
	if(debug && net->network == NULL)printf("WARNING: failed to allocate requested array\n");
	if(debug)printf("SYSTEM: setNetwork has finished successfully\n");
}

Network initNetwork(){
	Network net;
	net.size=0;
	net.network=NULL;
	return net;
}

void appendArray(Array* target, unsigned long element){
	if(debug)printf("SYSTEM: appendArray has been called with input values: ");
	if(debug)printf("	target(array size): %lu, element: %lu\n", target->size, element);
	if(debug)printf("SYSTEM: the contents of the target are:\n	");
	printArray(*target);

	unsigned long* holder=(unsigned long*)malloc((target->size+1) * sizeof(unsigned long));
	for(unsigned long i = 0; i < target->size; i++){
		holder[i]=target->array[i];
	}
	free(target->array);
	target->size++;
	target->array=holder;
	target->array[target->size - 1] = element;
	if(debug)printf("SYSTEM: array appended\n");
	if(debug)printf("SYSTEM: the contents of the target are:\n	");
	printArray(*target);
	if(debug)printf("SYSTEM: appendArray has finished successfully\n");
}

Array trimArray(Array dividend, Array measure){
	if(debug)printf("SYSTEM: trimArray has been called with input values: ");
	if(debug)printf("	dividend(array size): %lu, measure(array size): %lu\n",dividend.size, measure.size);
	if(debug)printf("SYSTEM: the contents of the dividend are:\n	");
	printArray(dividend);
	if(debug)printf("SYSTEM: the contents of the measure are:\n	");
	printArray(measure);
	bool present=false;
	Array output=initArray();
	for(unsigned long i = 0; i < dividend.size; i++){
		for(unsigned long j = 0; j < measure.size; j++) if(*(dividend.array + i) == *(measure.array + j))present = true;
		if(!present)appendArray(&output, *(dividend.array + i));
		present = false;
	}
	if(debug)printf("SYSTEM: trimArray has finished successfully\n");
	if(debug)printf("SYSTEM: the contents of the output are:\n	");
	printArray(output);
	return output;
}

unsigned long minArray(Array array){
	unsigned long min;
	for(unsigned long i = 0; i < array.size; i++){
		if(i==0 || array.array[i] < min){
			min=array.array[i];
		}
	}
	return 0;
}

void duplicateArray(Array reference, Array* target){
	if(debug)printf("SYSTEM: duplicateArray has been called\n");
	setArray(target, reference.size);//memorize the size of target
	for(int i = 0; i < reference.size; i++){//target is assumed to be empty and pre-allocated
		*(target->array+i) = *(reference.array+i);//copy contents of reference array into target array
	}
	if(debug)printf("SYSTEM: duplicateArray has finished successfully\n");
}

void removeArray(Array* input, unsigned long element){
	Array holder=initArray();
	for(unsigned long i = 0; i < input->size; i++){
		if(input->array[i] != element)appendArray(&holder, element);//append element to the holder array only if it is not the one we seek to remove
	}
	//and yoink all data over to the input array
	free(input->array);
	input->size = holder.size;
	input->array = holder.array;
}

void searchRemove(Array* arr, unsigned long element){
	Array h = initArray();
	for(unsigned long i = 0; i < arr->size; i++){
		if(arr->array[i] != element)appendArray(&h, arr->array[i]);
	}
	setArray(arr, 0);
	arr->size=h.size;
	arr->array=h.array;

}

float distance(int x1, int y1, int x2, int y2){
	float out = sqrt( (x1-x2)*(x1-x2) + (y1-y2)*(y1-y2) );
	if(debug)printf("SYSTEM: distance has been successfully called, and outputted %.2f\n", out);
	return out;
}
Node* getNodeByIndex(Network* net, unsigned long nodeID){
	if(debug)printf("SYSTEM: getNodeByID has been called with input values: ");
	if(debug)printf("	net(size): %lu, nodeID: %lu\n", net->size, nodeID);
	for(unsigned long i = 0; i < net->size; i++){
		if(net->network[i].id==nodeID){
			if(debug)printf("SYSTEM: node found! ");
			printNode(net->network[i]);
			if(debug)printf("SYSTEM: getNodeByID has finished successfully\n");
			return &net->network[i];
		}
	}
	if(debug)printf("WARNING: function failed to find the node with given id; returning NULL\n");
	return NULL;
}


unsigned long pathLength(Network nodes, Array path){
	if(debug)printf("SYSTEM: pathLength has been called\n");
	unsigned long out=0;
	Node lastNode=initNode();
	for (unsigned long i = 0; i < path.size; i++){
		if(i!=0)out += distance((nodes.network + i)->x,(nodes.network + i)->y,lastNode.x,lastNode.y);
		lastNode=*(nodes.network + i);
	}
	if(debug)printf("SYSTEM: pathLength has finished successfully with output value of %lu\n", out);
	return out;
}

Array findPath(unsigned long startID, unsigned long endID, Network nodes){
	if(startID == endID)return initArray();//return if the network is arbitrary
	Map map = setupMap(nodes);//prepare map
	if(debug)printf("SYSTEM: map has been configured\n");
	Array border=initArray();//initialize border
	appendArray(&border, startID);//add the starting node to the border
	if(debug)printf("system: border has been configured\n");
	while(!findTracker(map, endID)->visited && border.size > 0){//repeat until the path is found, or there are no more nodes to go through
		Tracker* min=NULL;
		//find the smallest node in the border
		if(debug)printf("SYSTEM: searching for the smallest node in the border\n");
		for(unsigned long i = 0; i < border.size; i++){//go through every node in the border
			Tracker* curr = findTracker(map, border.array[i]); //initialize current node
			if(min == NULL || min->weight > curr->weight )min = curr;//mark it as min if it is smaller than current min
		}
		if(debug)printf("SYSTEM: smallest node found: ");
		printNode(*getNodeByIndex(&nodes, min->node));
		if(debug)printf("\n");
		min->visited = true;//mark it as visited. NOTE: this line might not be neccesary, or moved outside of this while loop, and made start node specific
		removeArray(&border, min->node);//remove it from the border list
		Node* curr = getNodeByIndex(&nodes, min->node);
		for(unsigned long i = 0; i < curr->adjacent.size; i++){//go through every node adjacen to the current one
			Node* current = getNodeByIndex(&nodes, curr->adjacent.array[i]);//initialize the processed node
			Tracker* currentT = findTracker(map, current->id);
			if(debug)printf("SYSTEM: current node has been found:");
			if(debug)printNode(*current);
			if(debug)printf("\n");
			if( !currentT->visited ){//if the processed node hasn't been visited
				currentT->visited = true;//mark it as visited
				if( currentT->origin == 0 || currentT->weight > min->weight + distance(curr->x, curr->y, current->x, current->y)){//if the processed node has no origin node or its weight is greater than that if it used the current node
					currentT->origin = curr->id;//set its origin as the current node
					currentT->weight = min->weight + distance(curr->x, curr->y, current->x, current->y);//and set its weight to the origin
				}
			}
		}
	}
	if( !findTracker(map, endID)->visited )return initArray();//return an empty array if no path was found (aka. buffer is empty, and end node hasnt been visited)
	Array out=initArray();//initialize the output array
	appendArray(&out, endID);//add the end to it
	while( out.array[out.size-1]!=startID ){//while the start node is not in the array
		appendArray(&out, findTracker(map, out.array[out.size-1])->origin);//append the origin node of the current node
	}
	Array buff=initArray();
	setArray(&buff, out.size);
	unsigned long i = out.size;
	while(i-- > 0){
		buff.array[out.size-i] = out.array[i];//quickly inverse the array
	}
	return out;

}
